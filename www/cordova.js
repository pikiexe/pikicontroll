
setTimeout(()=>{
    document.dispatchEvent(new Event("deviceready"));
},50);

window.bluetoothSerial = {
    conn: false,
    err: false,
    call:()=>{},
    delimiter:null,

    isConnected: (s = () => { }, e = () => { }) => {
        console.log(`[bluetoothSerial] isConnected ... ${this.err ? "error" : "done"}`);
        this.conn?s():e();
    },
    write: (d, s = () => { }, e = () => { }) =>{
        console.log(`[bluetoothSerial] write: ${d} ... ${this.err ? "error" : "done"}`);
        if (this.err) return e();
        s();
    },
    list: (s = () => { }, e = () => { }) => {
        console.log(`[bluetoothSerial] list ... ${this.err ? "error" : "done"}`);
        if (this.err) return e();
        s([
            {id:"abc",name:"lol"+Math.random()},
            {id:"def0",name:"lul"},
            {id:"def1",name:"lul1"},
            {id:"def2",name:"lul2"},
            {id:"def3",name:"lul3"},
            {id:"def4",name:"lul4"},
            {id:"def5",name:"lul5"},
            {id:"def6",name:"lul6"},
            {id:"def7",name:"lul7"},
            {id:"def8",name:"lul8"},
            {id:"def9",name:"lul9"},
            {id:"def10",name:"lul10"},
            {id:"def11",name:"lul11"},
            {id:"def12",name:"lul12"},
            {id:"def13",name:"lul13"},
            {id:"def14",name:"lul14"},
            {id:"def15",name:"lul15"},
            {id:"def16",name:"lul16"},
            {id:"def17",name:"lul17"},
        ]);
    },
    connect: (d, s = () => { }, e = () => { }) => {
        console.log(`[bluetoothSerial] connect to: ${d} ... ${this.err?"error":"done"}`);
        if(this.err)return e();
        conn=true;
        setTimeout(s,2000);
    },
    disconnect: (s = () => { }, e = () => { }) => {
        console.log(`[bluetoothSerial] disconnect to ... ${this.err?"error":"done"}`);
        conn = false;
        if(this.err)return e();
        s();
    },
    subscribe: (d, f = () => { }, e = () => { }) => {
        console.log(`[bluetoothSerial] subscribe, delimiter: ${d} ... ${this.err?"error":"done"}`);
        if(this.err)return e();
        delimiter = d;
        call = f;
    },

};