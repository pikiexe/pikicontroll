import { Stear, SWindow, _ } from "./stear/main.js";
import * as ble from "./ble.js";
import connect from "./connect.js";
import { fadein, fadeout, subCancel } from "./stear/utils.js";
import showStatus from "./stear/extra/Pages/showStatus.js";
import backArrow from "./backArrow.js";

const pool = Stear.addLanguagePool("connectionList");
const refreshed = pool.add("refreshed", "Refreshed");
const refresh = pool.add("refresh", "Refresh");
const devicelist = pool.add("devicelist", "Device List");
const connectedto = pool.add("connectedto", "Connected to: {}");

const call = async (stear, { find, resolve, render, call, event, close }, { }) => {
    
    var cancel = subCancel(()=>resolve(false));
    var cancel2 = await call(backArrow, { callback: ()=>{
        console.log("callback");
        resolve(false);
    } });
    event.onloaded = () => {
        fadein(find.main, 200, true);
    };
    event.onresolve = async () => {
        await fadeout(find.main);
    };

    return _({
        style: {
            flexDirection: "column",
            display: "flex",
            height: "100%",
        }
    }, [
        _({
            style: {
                width: "100%",
                padding: "1em 0",
                borderBottom: "1px solid",
                textAlign: "center",
            }
        }, [
            _({ type: "b" }, devicelist),
            _({
                type: "button",
                style: {
                    border: "solid 1px",
                    borderRadius: "5px",
                    backgroundColor: "transparent",
                    outline: "none",
                    webkitUserSelect: "none",
                    position: "absolute",
                    right: "1em",
                },
                event:{click: async ()=>{
                    await render();
                    call(showStatus, { text: refreshed })

                }}
            }, refresh)
        ]),
        _({
            find: "list",
            style: {
                overflow: "hidden auto"
            }
        }, async ()=>(await ble.list()).map(device => _({                
                event:{click: async () => {
                    cancel();
                    cancel2();
                    if(await call(connect, { id: device.id, name: device.name })){
                        call(showStatus, { text: connectedto.r(device.name), color: "green" });
                        resolve([device.id, device.name]);
                    }else{
                        cancel = subCancel(() => resolve(false));
                        cancel2 = await call(backArrow, {
                            callback: () => {
                                console.log("callback");
                                resolve(false);
                            }
                        });
                        render();
                    }
                }}, 
                style: {
                    borderBottom: "1px solid #d3d3d3",
                    padding: "0.75rem 1rem 0.5rem 1rem ",
                    minHeight: "20px",
                    fontSize: "0.9em",
                }
            }, [
                _({ type: "b" }, device.name),
                _({ type: "br" }),
                device.id
            ])
        ))
    ]);
};

export default new SWindow({ call, backgroundColor: "#dde"});