import { Stear, SWindow, _ } from "./stear/main.js";

const call = async (stear, { find, resolve, render, call, event }, args) => {

    event.onloaded = () =>{

    }
    event.onresolve = async ()=>{
        
    }
    event.onclose = async ()=>{

    }
    event.onBeforRerender = async ()=>{

    }
    event.onAfterRerender = async ()=>{

    }
    //static Structure
    return _({

    }, [
        _({
            
        }, [
            
        ]) 
    ]);
    //complete dynamic structure
    return async ()=>_({

    }, [
        _({

        }, [
            
        ]), 
        ()=>_({ //sub function: possible but unnessesary in complete dynamic structure

        }, [
            
        ]) 
    ]);
    //partial dynamic structure
    return _({

    }, [
        ()=>_({

        }, [
            
        ]) 
    ]);
}

export default new SWindow({ preRender:true, call, backgroundColor: "#dde" });