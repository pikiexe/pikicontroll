import { onConnectionChange } from "./ble.js";
import connectionList from "./connectionList.js";
import showStatus from "./stear/extra/Pages/showStatus.js";
import { Stear, SWindow, _ } from "./stear/main.js";
import { fadein, wait } from "./stear/utils.js";

const pool = Stear.addLanguagePool("home");
const connected = pool.add("conncted","Connected to: {}");
const connect = pool.add("connect","Connect");
const connectionlost = pool.add("connectionlost","Connection lost from: {}");

const call = async (stear, { find, resolve, render, call, event }, {}) => {
    event.onloaded = async () => await fadein(find.main,200,true);

    onConnectionChange(({ state, id, name})=>{
        if (state) find.bar._.innerText = connected.r(name);
        else {
            find.bar._.innerText = connect;
            call(showStatus,{text:connectionlost.r(name), color:"orange"});
        }
    });

    return _({
        style: {
            flexDirection: "column",
            display: "flex",
            height: "100%",
        }
    }, [
        _({
            style: {
                width: "100%",
                padding: "1em 0",
                borderBottom: "1px solid",
                textAlign: "center",
            }
        }, [
            _({
                type: "b", find: "bar", event: {
                    click: async () => {
                        let d;
                        if (d = await call(connectionList, {})) {
                            find.bar._.innerText = connected.r(d[1]);
                        }
                    }
                }
            }, connect),
        ]),
        _({
            style: {
                flexDirection: "column",
                display: "flex",
            }
        }, [

        ])
    ]);    
}

export default new SWindow({ call, backgroundColor: "#dde"});