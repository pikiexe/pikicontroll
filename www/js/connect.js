import { connect } from "./ble.js";
import l1 from "./stear/extra/Elements/loading1.js";
import showStatus from "./stear/extra/Pages/showStatus.js";
import { Stear, SWindow, _ } from "./stear/main.js";
import { fadein, fadeout, wait } from "./stear/utils.js";

const pool = Stear.addLanguagePool("connect");
const couldnot = pool.add("couldnot", "Could not connect to: {}");
const connecting = pool.add("connecting", "Connecting to: {} ({})");

const call = async (stear, { find, resolve, render, call, close, event }, {name,id}) => {
    
    event.onloaded = async ()=>{
        fadein(find.main, 200, true); 
        try {
            await connect(id);

            resolve(true, false);
            await wait(300);
            await fadeout(find.main);
            close();
        } catch (error) {
            call(showStatus, { text: couldnot.r(name), color: "orange" });
            await fadeout(find.main);
            return resolve(false);
        }
    }  
    
    


    return _({}, [
        _({
            type: "p", 
            style: {
                position: "absolute",
                minWidth: "90%",
                textAlign: "center",
                top: "25%",
                left: "50%",
                transform: "translate(-50%,-50%)",
            }
        }, connecting.r(name,id)),
        _({
            style: {
                position: "absolute",
                left: "50%",
                top: "50%",
                transform: "translate(-50%, -50%)",
            }
        }, l1())
    ]);
}

export default new SWindow({ preRender:true, call, backgroundColor: "#dde" });