import { wait } from "./stear/utils.js";


var bluetoothSerial = window.bluetoothSerial;
var isConnectedV = false;

setInterval(()=>{
    if (bluetoothSerial == null) return;
    isConnected()
},500);

function getbleData(data){
    var dataa = data.split("\n");
    for (var i = 0; i < dataa.length; i++) {
        if (dataa[i] !== "") {
            func(dataa[i]);
        }
    }
}


//export const setHandler = (handler) => bluetoothSerial = handler;
var connectCache;

export const connect = (id) => {
    if (bluetoothSerial == null)throw new Error("bluetoothSerial Handler is not set!");
    disconnect();
    return new Promise((res,rej)=>{
        bluetoothSerial.connect(id, function () {
            connectCache = id;
            bluetoothSerial.subscribe('\n', getbleData, rej);
            isConnectedV = true;
            res();
        }, rej);
    });
}

export const disconnect = () => {
    bluetoothSerial.disconnect();
    isConnectedV = false;
};

var listCache = {};

export const list = () => {
    return new Promise((res,rej)=>{
        bluetoothSerial.list((data)=>{
            listCache = Object.fromEntries(data.map(d=>[d.id,d.name]));
            res(data);
        }, rej);
    });
}
export const isConnected = () => {
    return new Promise((res,rej)=>{
        bluetoothSerial.isConnected(()=>{
            if (!isConnectedV) connectionChanged();
            isConnectedV = true;
            res(true);
        }, () => {
            if (isConnectedV) connectionChanged();
            isConnectedV = false;
            res(false);
        });
    });
}

var tosend = [];

export const send = (data) => {
    if (!isConnectedV) return false;
    return new Promise((res,rej)=>{
        tosend.push({
            d: data,
            s: res, 
            f: rej 
        });
        sendData();
    });
};

var func = () => {};

export const onData = (f) => {
    func = f;
};

var changeCache = [];
export const onConnectionChange = (callback) => {
    changeCache.push(callback);
    return ()=>{
        var id;
        if ((id = changeCache.indexOf(callback)) >= 0) {
            changeCache.splice(id, 1);
        }
    };
};

function connectionChanged(){
    for (let i = 0; i < changeCache.length; i++) {
        let f;
        if (f = changeCache[i]) f({state: !isConnectedV, id:connectCache, name:listCache[connectCache]});        
    }
}

function PromiseSend(data){
    return new Promise((res,rej)=>{
        bluetoothSerial.write(data, res, rej);
    });
}
 
var isSending = false;
async function sendData(){
    if(isSending)return;
    isSending = true;
    while(tosend.length>0){
        let now = tosend.pop();
        let data = now.d;
        try {
            while(data.length>0){
                let now = data.substr(0, 490);
                data = data.substr(490);
                await PromiseSend(now + (data.length > 0 ? "}c" : "}e") + "\n");
                await wait(100);
            }
            now.s(true);
        } catch (e) {
            now.f();
        }
    }


    isSending = false;
}