import connect from "./connect.js";
import home from "./home.js";
import { Stear, SWindow, _ } from "./stear/main.js";
import { fadein, fadeout, wait } from "./stear/utils.js";

const preRender = true;

var wobel = Stear.addAnimation({
    "0%": {
        transform: "translate(-50%, -50%) scale(100%)"
    },
    "50%": {
        transform: "translate(-50%, -50%) scale(110%)"
    },
    "100%": {
        transform: "translate(-50%, -50%) scale(100%)"
    }
});

const call = async (stear, { find, resolve, render, call, event }, args) => {
    
    event.onloaded = async ()=>{
        await wait(Math.max(1, 1000 - performance.now()));
        await resolve();
        call(home);
    };
    event.onresolve = async()=>await fadeout(find.main);
    
    return _({ style: { zindex: 1000 }, find: "main" }, [
        _({
            type: "img", src: "/images/logo.png", style: {
                width: "80vw",
                maxWidth: "75vh",
                position: "absolute",
                left: "50%",
                top: "50%",
                transform: "translate(-50%, -50%)",
                animation: `${wobel} 2s ease-in-out 0s infinite`,

            }
        })
    ]);
}


export default new SWindow({ preRender:true, call, backgroundColor: "#dde" });