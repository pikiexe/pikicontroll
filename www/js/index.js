import connect from "./connect.js";
import connectionList from "./connectionList.js";
import start from "./start.js";
import { applyScrollBarCss1 } from "./stear/extra/Style/scrollBar1.js";
import { Stear, SWindow } from "./stear/main.js";
import showStatus from "./stear/extra/Pages/showStatus.js";
import alert from "./stear/extra/Pages/alert.js";

var stear = new Stear(document.querySelector("#stear"));

stear.style({
    backgroundColor: "#dde",
    fontFamily: "Arial, sans-serif",
    fontWeight: "lighter",
    fontSize: "0.9em",
    color: "#2a2a2a",
});
applyScrollBarCss1();

stear.addElement("connect", connect);
stear.addElement("start", start);
stear.addElement("connectionList", connectionList);

stear.call(stear.g("start"), {});

window.stear = stear;
window.Stear = Stear;
window.showStatus = showStatus;
window.alert = alert;

Stear.addLanguageFile({ 
    connectionList: { 
        refreshed: "Neugeladen", 
        refresh: "Neuladen", 
        devicelist: "Geräteliste", 
        connectedto: "Verbindung hergestellt mit: {}" 
    }, 
    home: { 
        conncted: "Verbunden mit: {}", 
        connect: "Verbinden", 
        connectionlost: "Verbindung verloren mit: {}" 
    },
    connect:{ 
        couldnot: 'Konnte keine Verbindung herstellen zu: {}', 
        connecting: 'Verbinde mit: {} ({})' 
    }
}, "de");
Stear.lang = "de";
