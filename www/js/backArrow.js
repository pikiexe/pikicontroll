import { SFrame, _ } from "./stear/main.js";
import { fadein, fadeout } from "./stear/utils.js";



const call = async (stear, { find, resolve, render, call, close, event }, { callback }) => {
    resolve( () => close(), false);

    event.onloaded = () => fadein(find.main, 200, true);
    event.onclose = async () => await fadeout(find.main);

    return _({ find: "main", style: { position: "absolute" } }, [
        _({
            src: "/images/pfeil.png", 
            type: "img", 
            style: {
                //zIndex: 1010,
                marginTop: "0.5rem",
                marginLeft: "0.5rem",
                height: "2rem",
            }, 
            event: {
                click: () => {
                    callback();
                    close();
                }
            }
        })
    ]);
}

export default new SFrame({ preRender: true, call });