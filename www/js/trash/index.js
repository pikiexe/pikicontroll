
document.addEventListener('deviceready', onDeviceReady, false);

async function onDeviceReady() {   
    
    ble = await import("./ble.js");
    ble.setHandler(bluetoothSerial);
    utils = await import("./utils.js");

    await utils.wait(Math.max(1, 1000 - performance.now()));

    home = await import("./main.js");
    
}
