export const wait = (ms) => {
    return new Promise((res, rej) => {
        setTimeout(res, ms);
    });
}

export const fadeout = async (elems,ms=200) => {
    if(!Array.isArray(elems))elems = [elems];
    for (let i = 0; i < elems.length; i++) {
        elems[i].style.transition = `opacity ${ms}ms`;
    }    
    await wait(1);
    for (let i = 0; i < elems.length; i++) {
        elems[i].style.opacity = "0";
    } 
    await wait(ms);
    for (let i = 0; i < elems.length; i++) {
        elems[i].style.display = "none"; 
    }   
}
export const fadein = async (elems,ms=200) => {
    if (!Array.isArray(elems)) elems = [elems];
    for (let i = 0; i < elems.length; i++) {
        elems[i].style.transition = `opacity ${ms}ms`;
        elems[i].style.display = "";  
    }
    await wait(10);
    for (let i = 0; i < elems.length; i++) {
        elems[i].style.opacity = "1";      
    }
    await wait(ms);   
}

const statusDiv = document.getElementById("statusDiv");
const statusList = [];
var statusActive = false;

export const setStatus = async(msg) => {
    statusList.push(msg);
    if(statusActive)return;
    statusActive = true;

    statusDiv.innerText = statusList.shift();

    await fadein(statusDiv);
    statusDiv.style.transition = "";

    var next;

    while(true){
        await wait(3000);

        next = statusList.shift();
        if(next==null)break;

        statusDiv.style.backgroundColor = "#ffe";
        await wait(200);
        statusDiv.style.backgroundColor = "";
        statusDiv.innerText = next;
    }

    await fadeout(statusDiv);

    statusActive = false;
};