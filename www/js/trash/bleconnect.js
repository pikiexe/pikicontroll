import * as ble from "./ble.js";
import { w } from "./main.js";
import { setStatus } from "./utils.js";

const text = document.querySelector("#bleconnecttext");

export const connectBle = async (id,name) => {
    try {
        text.innerText = "Connecting to: " + name + " (" + id + ")";
        await w.show(3);
        await ble.connect(id);
        setStatus("Connected to: " + name);
        return true;
    } catch (e) {
        setStatus("Error: Could not connect to: "+name);
        return false;
    }
    
};