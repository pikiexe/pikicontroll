import * as ble from "./ble.js";
import { connectBle } from "./bleconnect.js";
import {w} from "./main.js";
import { subBackArrow } from "./stearing.js";
import { setStatus } from "./utils.js";

const deviceList = document.getElementById("deviceList");

async function render(){
    var list = await ble.list();

    deviceList.innerHTML = "";
    list.forEach(function (device) {
        var listItem = document.createElement('li'),
            html = '<b>' + device.name + '</b><br/>' + device.id;
        listItem.innerHTML = html;
        listItem.onclick = function () {
            connect([device.id, device.name]);
        };
        deviceList.appendChild(listItem);
    });
}

var connect;

function waitSelectinon(){
    return new Promise((res,rej)=>{
        var cancel = subBackArrow(()=>rej());
        connect = (d)=>{
            cancel();
            res(d);
        };
    })
}

export const select = async () => {
    var state = 0;
    try{
        w.show(2);
        
        await render();

        let id,name;

        do{
            state = 1;
            [id, name] = await waitSelectinon();
            state = 2;
        } while (!(await connectBle(id, name)));

        state = 3;

        return [id,name];
    }catch(e){
        if(state==0) setStatus("Error: Could not load Blutooth Device List!");
        if(state==1) setStatus("Canceled");
        if(state==3) setStatus("Internal Error");
        return false;
    }    
};

document.getElementById("refreshButton").addEventListener("click",async ()=>{
    try {
        await render();
        setStatus("Refreshed");
    } catch (error) {
        setStatus("Error: Could not refresh Blutooth Device List!");
    }
});