import { wait, fadeout, fadein } from "./utils.js"
class RatioElements {
    #elem = [];
    constructor(...e){
        this.#elem = e;
        this.#elem.forEach((e,i)=>{
            if(i==0)return;
            e.style.display = "none";
            e.style.opacity = "0";
        });
    }
    static loadIds(...ids){
        return ids.map(i=>document.querySelector(i));
    }
    show(id){
        return new Promise(async (res,rej)=>{
            await fadeout(this.#elem, 200);
            await fadein(this.#elem[id],200);
            res();
        });
    }
}
export {RatioElements};

const bArrow = document.getElementById("backArrow");

var bStack = [];

function back(){
    if (bStack.length) {
        bStack.pop()();
        if (!bStack.length) fadeout(bArrow);
    }
}

bArrow.addEventListener("click",back);
document.addEventListener("backbutton", back, false);

export const subBackArrow = (callback) => {
    bStack.push(callback);
    fadein(bArrow); 
    return ()=>{
        var id;
        if ((id = bStack.indexOf(callback))>=0){
            bStack.splice(id,1);
            if (!bStack.length) fadeout(bArrow);
        }
    };
}

export const setTopBar = (msg,callback) => {

}