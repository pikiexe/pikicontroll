#!/bin/bash
textReset=$(tput sgr0)
textGreen=$(tput setaf 2)
message_info () {
  echo "${textGreen}[my-app]${textReset} $1"
}

message_info "Creating necessary directories..."
mkdir plugins
mkdir platforms

message_info "Adding platforms..."
# If using cordova, change to: cordova platform add android
cordova platform add android
#cordova platform add ios

message_info "Adding plugins..."
# If using cordova, change to: cordova plugin add
cordova plugin add cordova-plugin-bluetooth-serial